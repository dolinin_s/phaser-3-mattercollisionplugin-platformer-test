<?php if( !defined('SD') ) exit(0);
/**
 * Created by PhpStorm.
 * User: Sergey Dolinin
 * Date: 31.03.2019
 * Time: 14:27
 */
global $g_aConfig;

/* /////////////////////////////////////////////////////// BASE ///////////////////////////////////////////////////// */
$g_aConfig['site_host'] = "";
$g_aConfig['site_protocol'] = "http://";
$g_aConfig['sd_admin_version'] = "1.0";
$g_aConfig['admin_mail'] = "";
$g_aConfig['maxim_mail'] = "";

/* ///////////////////////////////////////////////////// DATABASE /////////////////////////////////////////////////// */
$g_aConfig['database']['hostname'] = "";
$g_aConfig['database']['port'] = 3306;
$g_aConfig['database']['username'] = "";
$g_aConfig['database']['password'] = "";
$g_aConfig['database']['name'] = "";

/* ////////////////////////////////////////////////////// ERROR ///////////////////////////////////////////////////// */
$g_aConfig['error']['show'] = 1;

