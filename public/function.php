<?php if( !defined('SD') ) exit(0);
/**
 * Created by PhpStorm.
 * User: Sergey Dolinin
 * Date: 05.04.2019
 * Time: 23:28
 */

/** ------------------------------------------------------ HEADER ------------------------------------------------------
 * @param $cTitle => page Title.
 * @param $cDescription => page Description.
 * @param $cKeywords => page Key words.
 * @param $aLoadPluginsCSS => Массив имен палигинов которые требуется загрузить на странице.
 * @param $fLoadThemeSite => true/false загружаем ли тему сайта.
 * @param $cBodyClass => класс для тега body.
 */
function pages_Header( $cTitle, $cDescription, $cKeywords, $aLoadPluginsCSS = ['all'], $fLoadThemeSite = false,
                            $cBodyClass = "public"  )
{
    echo '<!DOCTYPE HTML>';
    echo '<html lang="ru">';
    echo '<head>';
        echo '<meta charset="utf-8">';
        echo '<title>' . $cTitle . '</title>';
        echo '<meta name="description" content="' . $cDescription . '"> ';
        echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">';
        echo '<meta name="keywords" content="' . $cKeywords . '" />';
        echo '<meta name="copyright" content=""/>';
        echo '<meta name="Author" lang="ru" content="Sergey Dolinin" />';

        // Auto load CSS files
        pages_AutoLoadCSS ($aLoadPluginsCSS);
    echo '</head>';
    echo '<body class=' . $cBodyClass . '>';
}

/** ------------------------------------------- PAGES FOOTER -----------------------------------------------------------
 * @param null $aPlugins
 */
function pages_Footer( $aPlugins = null)
{
    if ( $aPlugins === null )
    {
        pages_AutoLoadJS(['jQuery']);
    }
    else
    {
        pages_AutoLoadJS($aPlugins);
    }
    echo '</body>';
    echo '</html>';
}

/** ------------------------------------------------- AUTO LOADS CSS ---------------------------------------------------
 * $SD_GLOBAL['PLUGINS'] => Имена всех плагинов.
 * @param $aLoadPlugins => Массив имен плагинов которые загружать. ['_jQuery', 'globals'] имена должны точно совпадать с
 * именами папок.
 **/
function pages_AutoLoadCSS ($aLoadPlugins)
{
    global $SD_GLOBAL;

    if ( $aLoadPlugins !== null )
    {
        if (is_array($aLoadPlugins))
        {
            // Загружаем все СSS файлы плагинов.
            if ($aLoadPlugins[0] === "ALL")
            {
                foreach ($SD_GLOBAL['PLUGINS'] as $Plugin)
                {
                    foreach (glob(DIR . '/plugins/' . $Plugin['NAME'] . '/*.css') as $Item) {
                        $cExp = explode('/', $Item);
                        $cExp = end($cExp);
                        echo '<link rel="stylesheet" href="/plugins/' . $Plugin['NAME'] . '/' . $cExp . '">';
                    }
                }
            }
            // Загружаем только выбранные
            else
            {
                foreach ($aLoadPlugins as $Plugin)
                {
                    foreach (glob(DIR . '/plugins/' . $Plugin . '/*.css') as $Item)
                    {
                        $cExp = explode('/', $Item);
                        $cExp = end($cExp);
                        echo '<link rel="stylesheet" href="/plugins/' . $Plugin . '/' . $cExp . '">';
                    }
                }
            }
        }
    }

    // Load page css
    if ($SD_GLOBAL['LANG'] == null)
    {
        if (sd_UrlSegment(0) != null )
        {
            $cPage = sd_UrlSegment(0);
        }
        else
        {
            $cPage = "home";
        }
    }
    else
    {
        if (sd_UrlSegment(1) != null )
        {
            $cPage = sd_UrlSegment(1);
        }
        else
        {
            $cPage = "home";
        }
    }


        foreach( glob(DIR . '/public/pages/' . $cPage . '/*.css') as $Item )
        {
            $cExp = explode('/', $Item);
            $cExp = end($cExp);
            echo '<link rel="stylesheet" href="/public/pages/' . $cPage . '/' . $cExp . '">';
        }

}

/** --------------------------------------------------  AUTO LOAD JS ---------------------------------------------------
 * @param array $aLoadPlugins
 */

function pages_AutoLoadJS ($aLoadPlugins = ['ALL'])
{
    global $SD_GLOBAL;

    if ($aLoadPlugins !== null) {
        if ( is_array($aLoadPlugins) )
        {
            // Загружаем все JS файлы плагинов.
            if ($aLoadPlugins[0] === "ALL") {
                foreach ($SD_GLOBAL['PLUGINS'] as $Plugin) {
                    foreach (glob(DIR . '/plugins/' . $Plugin['NAME'] . '/*.js') as $Item) {
                        $cExp = explode('/', $Item);
                        $cExp = end($cExp);
                        echo '<script src="/plugins/' . $Plugin['NAME'] . '/' . $cExp . '"></script>';
                    }
                }
            }
            // Загружаем только выбранные
            else
            {
                foreach ($aLoadPlugins as $Plugin) {
                    foreach (glob(DIR . '/plugins/' . $Plugin . '/*.js') as $Item) {
                        $cExp = explode('/', $Item);
                        $cExp = end($cExp);
                        echo '<script src="/plugins/' . $Plugin . '/' . $cExp . '"></script>';
                    }
                }
            }
        }
    }

    // Load page js
    if ( $SD_GLOBAL['LANG'] == null )
    {
        if (sd_UrlSegment(0) != null)
        {
            $cPage = sd_UrlSegment(0);
        }
        else
        {
            $cPage = "home";
        }
    }
    else
    {
        if ( sd_UrlSegment(1) != null )
        {
            $cPage = sd_UrlSegment(1);
        }
        else
        {
            $cPage = "home";
        }
    }

    $cPathURL = DIR . '/public/pages/' . $cPage . '';
    foreach (glob($cPathURL . "/*") as $filename)
    {
        $aInfo = pathinfo($filename);
        if ($aInfo['extension'] === "js")
        {
            $cExp = explode('/', $filename);
            $cExp = end($cExp);
            echo '<script src="/public/pages/' . $cPage . '/' . $cExp . '"></script>';
        }

        if ( is_dir($filename) )
        {
            foreach (glob($filename . "/*.js") as $fileJS)
            {
                $cExp = explode('/', $fileJS);
                $cExpEnd = end($cExp);
                $cExpStart = prev($cExp);
                echo '<script src="/public/pages/' . $cPage . '/' . $cExpStart . '/' . $cExpEnd .'"></script>';
            }
        }
    }
}