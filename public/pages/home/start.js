/**
 * Start file
 * Create config game
 * Phaser start
 */

GLOBALS.CONFIG =
{
    type: Phaser.AUTO,
    width: 1366,
    height: 825,
    debug: false,
    backgroundColor: "#fffbf9",
    scene: new Menu,
    parent: "game-container",
    physics: {default: "matter"},
    pixelArt: true,
    dom: {
        createContainer: true
    },
    plugins: {
        // matterCollision не работает с версии 3.22.0 (https://phaser.io/download/release/3.22.0)
        scene: [
            {
                plugin: PhaserMatterCollisionPlugin,    // The plugin class
                key: "matterCollision",                 // Where to store in Scene.Systems, e.g. scene.sys.matterCollision
                mapping: "matterCollision"              // Where to store in the Scene, e.g. scene.matterCollision
            }
        ]
    }
};

GLOBALS.GAME = new Phaser.Game(GLOBALS.CONFIG);
