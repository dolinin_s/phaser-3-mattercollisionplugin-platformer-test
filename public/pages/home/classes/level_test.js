/** ---------------------------------------------- LEVEL TEST ------------------------------------------------------ **/
class Level_test extends Phaser.Scene
{
    constructor() {
        super(
            {
                key: 'LEVEL_TEST'
            }
        );
    }

    preload ()
    {
        console.log('-----------Start level------------');

        GLOBALS.SCENE.LEVEL_TEST = this;

        // Lvl background
        this.load.image("level_1_bg", GLOBALS.PATH.BACKGROUNDS + "forest.png");

        // Map
        this.load.tilemapTiledJSON("level_test", GLOBALS.PATH.TILEMAPS + "level_test/1.9.json");
        this.load.image("tileset_4", GLOBALS.PATH.TILEMAPS + "level_test/tileset_4.png");
    }

    create ()
    {
        let nCameraHorizontalCenter = parseInt(GLOBALS.SCENE.LEVEL_TEST.scale.width / 2);
        let nCameraVerticalCenter = parseInt(GLOBALS.SCENE.LEVEL_TEST.scale.height / 2);

        ////////////////////////////////////////// MAP CREATE //////////////////////////////////////////////////////////
        let map = this.make.tilemap({ key: "level_test" });
        let tileset = map.addTilesetImage("tileset_4");
        this.matter.world.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
        this.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);

        // Background
        let objBg = GLOBALS.SCENE.LEVEL_TEST.add.image(nCameraHorizontalCenter, nCameraVerticalCenter, 'level_1_bg');
        objBg.setDepth('-10');
        objBg.setOrigin(0.5);
        objBg.setScrollFactor(0);

        // Ground
        let groundLayer = map.createDynamicLayer("ground", tileset, 0, 0);
        groundLayer.setDepth(7);
        groundLayer.setCollisionByProperty({ collides: true });
        this.matter.world.convertTilemapLayer(groundLayer);

        // Debug
        //this.matter.world.createDebugGraphic();

        //  Tree
        let decoration = map.createDynamicLayer("tree", tileset, 0, 0);
        decoration.setDepth(4);

        //  Decoration
        let decoration1 = map.createDynamicLayer("dec_1", tileset, 0, 0);
        decoration1.setDepth(15);

        //  Decoration2
        let decoration2 = map.createDynamicLayer("dec_2", tileset, 0, 0);
        decoration2.setDepth(15);

        //  Platform
        let platform = map.createDynamicLayer("platforms", tileset, 0, 0);
        platform.setDepth(6);
        platform.setCollisionByProperty({ collides: true });
        this.matter.world.convertTilemapLayer(platform);

        // House
        let objHouse_bottom = map.createStaticLayer("house_nocolid", tileset, 0, 0);
        objHouse_bottom.setDepth(6);

        let objHouse_top = map.createStaticLayer("house_col", tileset, 0, 0);
        objHouse_top.setDepth(10);
        objHouse_top.setCollisionByProperty({ collides: true });
        this.matter.world.convertTilemapLayer(objHouse_top);

        // Точка спауна игрока
        const { x, y } = map.findObject("Objects", obj => obj.name === "Spawn Point");

        // Создаем игрока
        //this.player = new PlayerKnight (GLOBALS.SCENE.level_1, x, y);
        this.player = new PlayerGoblin(GLOBALS.SCENE.LEVEL_TEST, x, y);

        // Smoothly follow the player
        this.cameras.main.startFollow(this.player.sprite, false, 5, 5);

        // Область события
        const eventArea = map.findObject("Sensors", obj => obj.name === "Finish");
        const FinishSensor = GLOBALS.SCENE.LEVEL_TEST.matter.add.rectangle (
            eventArea.x + eventArea.width / 2,
            eventArea.y + eventArea.height / 2,
            eventArea.width,
            eventArea.height,
            {
                isSensor: true, // Он не должен физически взаимодействовать с другими телами
                isStatic: true  // Не должно двигаться
            }
        );

        GLOBALS.SCENE.LEVEL_TEST.welcomeMessange = this.matterCollision.addOnCollideStart(
        {
                objectA: this.player.sprite,
                objectB: FinishSensor,
                //callback: Level_test.showPlayerDialog,
                callback: function(eventData) {
                    let dialog = new PlayerDialog(this,'Welcome');
                    dialog.show();
                },
                context: this
        });

        // Добавляем Update
        this.events.on("update", this.update, this);

        // Исправляет кровотечение текстур (В самом конце)
        this.cameras.main.roundPixels = true;
    }

    update ()
    {

    }
}
