/** --------------------------------------------------- MENU -----------------------------------------------------------
 *  Game start scene
 */
class Menu extends Phaser.Scene
{
    constructor()
    {
        super(
            {
                key: 'Menu',
                pack:
                {
                    files:
                    [{
                        type: 'plugin',
                        key: 'rexwebfontloaderplugin',
                        url: '/plugins/WebFontLoader/rexwebfontloaderplugin.min.js',
                        start: true
                    }]
                }
            }
        );
    }

    //------------------------------------------------- PRELOAD --------------------------------------------------------
    preload()
    {

        GLOBALS.SCENE.MENU = this;
        menu_preload();

        this.plugins.get('rexwebfontloaderplugin').addToScene(this);

        // Подгружаем шрифты
        let objFontsConfig = { custom: { families: ['RussoOne-Regular, NotoSans'], urls: ['/public/pages/home/style.css'] } };
        this.load.rexWebFont(objFontsConfig);

        this.load.image("background", GLOBALS.PATH.MENU + "/menu.jpg");
        this.load.image("btn_1", GLOBALS.PATH.MENU + "/btn_1.png");
        this.load.image("btn_1_hover", GLOBALS.PATH.MENU + "/btn_1_hover.png");
        this.load.html("name_form", GLOBALS.PATH.HTML + "/input_name.html");

        // Player knight
        this.load.atlas("player_knight", GLOBALS.PATH.SPRITES + "/knight/player_knight.png",
            GLOBALS.PATH.SPRITES + "/knight/player_knight.json");

        // Player goblin
        this.load.atlas("player_goblin", GLOBALS.PATH.SPRITES + "/goblin/player_goblin.png",
            GLOBALS.PATH.SPRITES + "/goblin/player_goblin.json");

    }

    //-------------------------------------------------- UPDATE --------------------------------------------------------
    update()
    {

    }

    //--------------------------------------------------- CREATE -------------------------------------------------------
    create()
    {
        let nCameraHorizontalCenter = parseInt(GLOBALS.SCENE.MENU.scale.width / 2);
        let nCameraVerticalCenter = parseInt(GLOBALS.SCENE.MENU.scale.height / 2);

        ///////////////////////////////////////////// BACKGROUND ///////////////////////////////////////////////////////
        let objMenuBackground = this.add.image(nCameraHorizontalCenter, nCameraVerticalCenter, 'background');
        //objMenuBackground.setScale(1);

        //////////////////////////////////////////////// MENU //////////////////////////////////////////////////////////
        let nMenuFontSize = 30;
        let nBtnHorizontalAlign = 300;
        let nBtnTextureDepth = 25;
        let nBtnTextDepth = 40;

        // * Btn Level test
        let nBtnTestTopAlign = nCameraVerticalCenter + 50;

        let objBtnLevel_test = GLOBALS.SCENE.MENU.add.image(nCameraHorizontalCenter + nBtnHorizontalAlign,
            nBtnTestTopAlign, 'btn_1');
        objBtnLevel_test.setDepth(nBtnTextureDepth).setOrigin(0.5).setScale(1);
        objBtnLevel_test.setInteractive({ useHandCursor: true  });
        objBtnLevel_test.on('pointerdown', function () { menu_startTestLevel(); });
        objBtnLevel_test.on('pointerover', function () { this.setTexture('btn_1_hover'); });
        objBtnLevel_test.on('pointerout', function () { this.setTexture('btn_1'); });

        let objBtnTextLevel_test = GLOBALS.SCENE.MENU.add.text(nCameraHorizontalCenter + nBtnHorizontalAlign,
            nBtnTestTopAlign, 'Test level', { fontFamily: 'RussoOne-Regular' });
        objBtnTextLevel_test.setFontSize(nMenuFontSize).setOrigin(0.5).setDepth(nBtnTextDepth);
        objBtnTextLevel_test.setShadow(-5, 5, 'rgba(0,0,0,0.3)', 0);

        // * Btn level one
        let nBtnLevelOne = nCameraVerticalCenter + 160;

        let objBtnLevel_one = GLOBALS.SCENE.MENU.add.image(nCameraHorizontalCenter + nBtnHorizontalAlign,
            nBtnLevelOne, 'btn_1');
        objBtnLevel_one.setDepth(nBtnTextureDepth).setOrigin(0.5).setScale(1);
        objBtnLevel_one.setInteractive({ useHandCursor: true  });
        objBtnLevel_one.on('pointerdown', function () { menu_startLevelOne(); });
        objBtnLevel_one.on('pointerover', function () { this.setTexture('btn_1_hover'); });
        objBtnLevel_one.on('pointerout', function () { this.setTexture('btn_1'); });

        let objBtnTextLevel_One = GLOBALS.SCENE.MENU.add.text(nCameraHorizontalCenter + nBtnHorizontalAlign,
            nBtnLevelOne, 'Level 1', { fontFamily: 'RussoOne-Regular' });
        objBtnTextLevel_One.setFontSize(nMenuFontSize).setOrigin(0.5).setDepth(nBtnTextDepth);
        objBtnTextLevel_One.setShadow(-5, 5, 'rgba(0,0,0,0.3)', 0);

        /*
        let objForm  = GLOBALS.SCENE.MENU.add.dom(nCameraHorizontalCenter, nCameraVerticalCenter).createFromCache('name_form');
        console.log(objForm);
        */

    }

}

//------------------------------------------------- START TEST LEVEL ---------------------------------------------------
function menu_startTestLevel ()
{
    // Если сцену еще не создали
    if (GLOBALS.SCENE.LEVEL_TEST === null)
    {
        // Stop MainScene
        GLOBALS.SCENE.MENU.scene.stop();

        let objLevel = new Level_test;
        GLOBALS.GAME.scene.add(objLevel, {key: 'level_test', preload: objLevel.preload, create: objLevel.create,
            update: objLevel.update}, true);
    }
}

//------------------------------------------------- START LEVEL ONE ----------------------------------------------------
function menu_startLevelOne ()
{
    // Если сцену еще не создали
    if (GLOBALS.SCENE.LEVEL_ONE === null)
    {
        // Stop MainScene
        GLOBALS.SCENE.MENU.scene.stop();

        let objLevel = new Level_one;
        GLOBALS.GAME.scene.add(objLevel, {key: 'level_1', preload: objLevel.preload, create: objLevel.create,
            update: objLevel.update}, true);
    }
}

//-------------------------------------------------- GAME PRELOAD ------------------------------------------------------
function menu_preload()
{
    let progressBar = GLOBALS.SCENE.MENU.add.graphics();
    let progressBox = GLOBALS.SCENE.MENU.add.graphics();
    let nBarWidth = 320;
    let nBarHeight = 50;
    progressBox.fillStyle(0x222222, 0.8);
    progressBox.fillRect(parseInt(GLOBALS.SCENE.MENU.scale.width/2 - nBarWidth/2),
        parseInt(GLOBALS.SCENE.MENU.scale.height/2 - nBarHeight/2), nBarWidth, nBarHeight);

    let width = GLOBALS.SCENE.MENU.cameras.main.width;
    let height = GLOBALS.SCENE.MENU.cameras.main.height;
    let loadingText = GLOBALS.SCENE.MENU.make.text({x: GLOBALS.SCENE.MENU.scale.width/2, y: GLOBALS.SCENE.MENU.scale.height / 2,
        text: 'Loading...', style: { font: '20px monospace', fill: '#fff' }}).setOrigin(0.5, 0.5);

    let percentText = GLOBALS.SCENE.MENU.make.text({
        x: GLOBALS.SCENE.MENU.scale.width/2,
        y: GLOBALS.SCENE.MENU.scale.height / 2 + 50,
        text: '0%',
        style: {
            font: '18px monospace',
            fill: '#000'
        }
    }).setOrigin(0.5, 0.5);

    let assetText = GLOBALS.SCENE.MENU.make.text({
        x: width / 2,
        y: height / 2 + 80,
        text: '',
        style: {
            font: '18px monospace',
            fill: '#000'
        }
    }).setOrigin(0.5, 0.5);

    GLOBALS.SCENE.MENU.load.on('progress', function (value) {
        percentText.setText(parseInt(value * 100) + '%');
        progressBar.clear();
        progressBar.fillStyle(0x27c004, 1);
        progressBar.fillRect(GLOBALS.SCENE.MENU.scale.width/2 - nBarWidth/2, GLOBALS.SCENE.MENU.scale.height/2 - nBarHeight/2, 300 * value, 49);
    });

    GLOBALS.SCENE.MENU.load.on('fileprogress', function (file) {
        assetText.setText('Loading asset: ' + file.key);
    });

    GLOBALS.SCENE.MENU.load.on('complete', function () {
        progressBar.destroy();
        progressBox.destroy();
        loadingText.destroy();
        percentText.destroy();
        assetText.destroy();
    });

}

