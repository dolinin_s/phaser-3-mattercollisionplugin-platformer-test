/** ---------------------------------------------- LEVEL 1 --------------------------------------------------------- **/
class Level_one extends Phaser.Scene
{
    constructor() {
        super(
            {
                key: 'LEVEL_ONE'
            }
        );
    }

    preload ()
    {
        console.log('-----------Start level------------');

        GLOBALS.SCENE.LEVEL_ONE = this;

        // Lvl background
        this.load.image("level_1_bg", GLOBALS.PATH.BACKGROUNDS + "forest.png");

        // Map
        this.load.tilemapTiledJSON("level_one", GLOBALS.PATH.TILEMAPS + "level_one/level_one_3.json");
        this.load.image("grounds", GLOBALS.PATH.TILEMAPS + "level_one/grounds.png");
        this.load.image("water", GLOBALS.PATH.TILEMAPS + "level_one/water.png");
        this.load.image("traps", GLOBALS.PATH.TILEMAPS + "level_one/traps.png");
        this.load.image("wooden_box", GLOBALS.PATH.OBJECTS + "/wooden_box.png");
        this.load.image("wooden_box_extruded", GLOBALS.PATH.OBJECTS + "/wooden_box_extruded.png");

        // Load body shapes from JSON file generated using PhysicsEditor
        this.load.json('shapes', GLOBALS.PATH.OBJECTS + '/shapes.json');

    }

    create ()
    {
        let nCameraHorizontalCenter = parseInt(GLOBALS.SCENE.LEVEL_ONE.scale.width / 2);
        let nCameraVerticalCenter = parseInt(GLOBALS.SCENE.LEVEL_ONE.scale.height / 2);
        let nStartDepthMap = 0;
        let nStartDepthObjects = 50;        // Игровые объекты всегда впереди

        ////////////////////////////////////////// MAP CREATE //////////////////////////////////////////////////////////
        let map = this.make.tilemap({ key: "level_one" });
        let grounds = map.addTilesetImage("grounds","grounds",128,128,5,8,0);
        let water = map.addTilesetImage("water","water",128,128,5,8,0);
        let traps = map.addTilesetImage("traps","traps",128,128,5,5,0);
        this.matter.world.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
        this.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);

        // Background
        let objBg = GLOBALS.SCENE.LEVEL_ONE.add.image(nCameraHorizontalCenter, nCameraVerticalCenter, 'level_1_bg');
        objBg.setDepth(nStartDepthMap);
        objBg.setOrigin(0.5);
        objBg.setScrollFactor(0);

        // Ground
        let objGroundLayer = map.createDynamicLayer("ground", grounds, 0, 0);
        objGroundLayer.setDepth(GLOBALS.PLAYER.DEPTH + 1);
        objGroundLayer.setCollisionByProperty({ collides: true });
        this.matter.world.convertTilemapLayer(objGroundLayer);

        // Tree
        //let objTree = map.createDynamicLayer("tree", grounds, 0, 0);
        //objTree.setDepth(nStartDepthMap + 10);

        // Spikes
        let objSpikes = map.createDynamicLayer("spikes", traps, 0, 0);
        objSpikes.setDepth(nStartDepthMap + 5);
        objSpikes.setCollisionByProperty({ collides: true });
        this.matter.world.convertTilemapLayer(objSpikes);

        // Decorations
        //let objMapDecorations = map.createDynamicLayer("decoration_elements", grounds, 0, 0);
        //objMapDecorations.setDepth(nStartDepthMap + 30);

        // Platforms
        let objMapPlatforms = map.createDynamicLayer("platforms", grounds, 0, 0);
        objMapPlatforms.setDepth(nStartDepthMap + 30);
        objMapPlatforms.setCollisionByProperty({ collides: true });
        this.matter.world.convertTilemapLayer(objMapPlatforms);

        // Water
        let objMapWater = map.createDynamicLayer("water", water, 0, 0);
        objMapWater.setDepth(GLOBALS.PLAYER.DEPTH + 1);

        // House
        //let objMapHouse = map.createDynamicLayer("house", grounds, 0, 0);
        //objMapHouse.setDepth(nStartDepthMap + 10);


        ////////////////////////////////////////////// OBJECTS /////////////////////////////////////////////////////////

        // Boxes
        let shapes = this.cache.json.get('shapes');
        let objBoxes = map.getObjectLayer("boxes").objects;
        objBoxes.forEach(function (objElement)
        {
            let x = parseFloat(objElement['width'] / 2) + objElement['x'];
            let y = parseFloat(objElement['height'] / 2) + objElement['y'];
            let objBox = GLOBALS.SCENE.LEVEL_ONE.matter.add.image(x, y, 'wooden_box_extruded','',{shape: shapes.wooden_box_extruded})
                .setMass(200)
                .setDepth(nStartDepthObjects);
            objBox.body.slop = 0.5;
        });
        

        /////////////////////////////////////////////// PLAYER /////////////////////////////////////////////////////////

        let aSpawnPoint = map.findObject("points", obj => obj.name === "spawn point");
        this.player = new PlayerGoblin(GLOBALS.SCENE.LEVEL_ONE, aSpawnPoint['x'], aSpawnPoint['y']);

        // Smoothly follow the player
        this.cameras.main.startFollow(this.player.sprite, false, 5, 5);

        // Исправляет кровотечение текстур (В самом конце)
        this.cameras.main.roundPixels = true;

        // Debug
        //this.matter.world.createDebugGraphic();
    }

    update ()
    {

    }
}
