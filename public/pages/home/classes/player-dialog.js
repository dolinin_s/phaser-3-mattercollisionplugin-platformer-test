/** ------------------------------------------------ PLAYER DIALOG WINDOW ------------------------------------------ **/
class PlayerDialog
{
    constructor(objScene, cDialogName)
    {
        this.scene = objScene;
        this.name = cDialogName;
        this.bEventClose = false;
        this.elementGroup = this.scene.add.group();
        this.cStatus = "create";

        this.nPlayerPosX = this.scene.player.sprite.x;
        this.nPlayerPosY = this.scene.player.sprite.y;
        this.nWindowHeight = 100;
        this.nWindowWidth = 800;
        this.nWindowMarginBottom = 20;
        this.nSceneHeight = GLOBALS.CONFIG.height;
    }

    show()
    {
        // Исключаем повторение открытие окна
        if ( GLOBALS.DIALOG.OPENED === null )
        {
            let objThisDialog = this;

            objThisDialog.cStatus = "process";
            GLOBALS.DIALOG.OPENED = objThisDialog.name;

            // Scene update
            objThisDialog.scene.events.on("update", this.update, this);

            // Create graphic
            let objGraphic = objThisDialog.scene.add.graphics();
            objThisDialog.elementGroup.add(objGraphic);

            // Window border
            objGraphic.setDefaultStyles({
                lineStyle: {width: 6, color: 0x663300, alpha: 1},
                fillStyle: {color: 0x663300, alpha: 1}
            });

            let objRectBorder = objGraphic.strokeRect(objThisDialog.nPlayerPosX - (objThisDialog.nWindowWidth / 2),
                objThisDialog.nSceneHeight - objThisDialog.nWindowHeight - objThisDialog.nWindowMarginBottom,
                objThisDialog.nWindowWidth, objThisDialog.nWindowHeight);
            objRectBorder.setDepth(75);
            objThisDialog.elementGroup.add(objRectBorder);

            // Window body
            objGraphic.setDefaultStyles({
                lineStyle: {width: 1, color: 0xcc9900, alpha:1},
                fillStyle: {color: 0xcc9900, alpha: 1}
            });
            let objRectBody = objGraphic.fillRect(objThisDialog.nPlayerPosX - (objThisDialog.nWindowWidth / 2),
                objThisDialog.nSceneHeight - objThisDialog.nWindowHeight - objThisDialog.nWindowMarginBottom,
                objThisDialog.nWindowWidth, objThisDialog.nWindowHeight);
            objRectBody.setDepth(70);
            objThisDialog.elementGroup.add(objRectBody);

            // Message
            let showText = objThisDialog.scene.add.text( objThisDialog.nPlayerPosX - (objThisDialog.nWindowWidth / 2),
                    objThisDialog.nSceneHeight - objThisDialog.nWindowHeight - objThisDialog.nWindowMarginBottom,
                        'Hello world!'
            );
            showText.setDepth(90);
            objThisDialog.elementGroup.add(showText);

            // Btn OK
            var objBtnConfig = {
                x: objThisDialog.nPlayerPosX + (objThisDialog.nWindowWidth / 2) - 5,
                y: objThisDialog.nSceneHeight - (objThisDialog.nWindowHeight/2) - 5,
                text: 'close',
                style: {
                    fontSize: '16px',
                    fontFamily: 'Arial',
                    color: '#ffffff',
                    align: 'center',
                    padding: {x: 10, y:5},
                    backgroundColor: '#ff00ff',
                    shadow: {
                        color: '#000000',
                        fill: true,
                        offsetX: 2,
                        offsetY: 2,
                        blur: 8
                    }
                }
            };

            let objBtnOK =  objThisDialog.scene.make.text(objBtnConfig).setDepth(90).setOrigin(1,0);
            objBtnOK.setInteractive();
            objBtnOK.on('pointerup', function(pointer, localX, localY, event) {
                objThisDialog.bEventClose = true;
            });
            objThisDialog.elementGroup.add(objBtnOK);

        }
        else
        {
            console.log('это окно уже было открыто');
        }

    }

    // Закрываем окно
    close()
    {
        this.elementGroup.clear(true);
        this.bEventClose = false;
        this.scene.player.freezeOff();
    }

    update()
    {
        // Только в процессе
        if ( this.cStatus === "process" )
        {
            this.scene.player.sprite.anims.play("player-idle", true);

            // Если игрок на земле только тогда его фризим !!!!
            if ( this.scene.player.isTouching.mainBody === true )
            {
                this.scene.player.sprite.anims.stop();
                this.scene.player.freeze();
            }

            // Закрываем окно диалога
            if ( this.bEventClose === true )
            {
                this.cStatus = "closed";
                this.close();
            }
        }
    }
}