/** ------------------------------------------------ PLAYER -------------------------------------------------------- **/
class PlayerGoblin
{
    constructor(scene, x, y)
    {
        this.scene = scene;
        this.sprite = scene.matter.add.sprite(0, 0, 'player_goblin');
        this.sprite.setDepth(GLOBALS.PLAYER.DEPTH);

        // Set characteristics
        this.characteristics = {};
        this.characteristics.cName = "Goblin";
        this.characteristics.nDefaultFriction = 0.26;                   // Трение по умолчанию
        this.characteristics.nLeftOrRightSensorsFriction = 0.001;       // Трение с обьектами когда падаем и цепляемся (лево, право).
        this.characteristics.nfrictionAir = 0;                          // The air friction of the body (air resistance).
        this.characteristics.nfrictionStatic = 0;                       // The static friction of the body (in the Coulomb friction model).
        this.characteristics.health = 5;                                // The static friction of the body (in the Coulomb friction model).


        // Add params
        this.isTouching = { ground: false };                            // Track which sensors are touching something
        this.bCanJump = true;                                           // Jumping is going to have a cooldown

        //*************************************** Animation ************************************************************
        const anims = scene.anims;
        let cLastAnims = "";        // Ключ последней проигранной анимации.

        anims.create({
            key: "player-idle",
            frames: anims.generateFrameNames("player_goblin", {prefix: 'goblin_idle_', start: 1, end: 18}),
            frameRate: 18,
            repeat: 0
        });
        anims.create({
            key: "player-idle-blinking",
            frames: anims.generateFrameNames("player_goblin", {prefix: 'goblin_idle_b_', start: 1, end: 18}),
            frameRate: 18,
            repeat: 0
        });
        anims.create({
            key: "player-run",
            frames: anims.generateFrameNames("player_goblin", {prefix: 'goblin_running_', start: 1, end: 12}),
            frameRate: 25,
            repeat: -1
        });
        anims.create({
            key: "player-falling-down",
            frames: anims.generateFrameNames("player_goblin", {prefix: 'goblin_falling_', start: 1, end: 6}),
            frameRate: 20,
            repeat: 0
        });
        anims.create({
            key: "player-jump-start",
            frames: anims.generateFrameNames("player_goblin", {prefix: 'goblin_jump_start_', start: 1, end: 6}),
            frameRate: 36,
            repeat: 0
        });
        anims.create({
            key: "player-jump-loop",
            frames: anims.generateFrameNames("player_goblin", {prefix: 'goblin_jump_loop_', start: 1, end: 6}),
            frameRate: 12,
            repeat: 0
        });

        //***************************************** Player structure ***************************************************
        const { Body, Bodies } = Phaser.Physics.Matter.Matter; // Native Matter modules
        const { width: w, height: h } = this.sprite;

        // Обьект с сенсорами ( тело, нижний сенсор, левый сенсор, правый сенсор )
        this.sensors =
        {
            mainBody:Bodies.fromVertices(0, 0,[ { "x":82, "y":53 }, { "x":64, "y":70 }, { "x":84, "y":160},
                        { "x":116, "y":160 }, { "x":133.14285278320312, "y":70 }, { "x":114, "y":53 } ]),
            bottom: Bodies.rectangle(0, h/2 - 38, 33, 5, { isSensor: true }),
            left: Bodies.rectangle(-40, 0 , 5, 70, { isSensor: true }),
            right: Bodies.rectangle(+40, 0, 5, 70, { isSensor: true })
        };

        // Фиксим высоту тела
        this.sensors.mainBody.position.y += 30;

        // Задаем имена для консоли, чтобы понимать с чем имеем дело.
        this.sensors.bottom.AName = "Bottom sensor";
        this.sensors.left.AName = "Left sensor";
        this.sensors.right.AName = "Right sensor";
        this.sensors.mainBody.AName = "Body sensor";

        const compoundBody = Body.create({
            parts:
            [
                this.sensors.mainBody,
                this.sensors.bottom,
                this.sensors.left,
                this.sensors.right
            ],
            frictionStatic: this.characteristics.nfrictionStatic,
            frictionAir: this.characteristics.nfrictionAir,
            friction: this.characteristics.nDefaultFriction
        });

        this.sprite
            .setExistingBody(compoundBody)
            //.setScale(1)
            .setFixedRotation() // Sets inertia to infinity so the player can't rotate
            .setPosition(x, y);


        // Before matter's update, reset our record of which surfaces the player is touching.
        scene.matter.world.on("beforeupdate", this.resetTouching, this);

        scene.matterCollision.addOnCollideStart({
            objectA: [this.sensors.bottom, this.sensors.left, this.sensors.right, this.sensors.mainBody],
            callback: this.onSensorCollide,
            context: this
        });
        scene.matterCollision.addOnCollideActive({
            objectA: [this.sensors.bottom, this.sensors.left, this.sensors.right, this.sensors.mainBody],
            callback: this.onSensorCollide,
            context: this
        });

        // Контролим анимацию по окончанию.
        // Если анимация бесконечная она сюда попадает при смене на другую анимацию!
        // Тут мы можем отследить конец любой анимации игрока.
        this.sprite.on('animationcomplete', function (anim, frame) {
            //console.log(anim.key);
            cLastAnims = anim.key;
        }, this.sprite);

        // Track the keys
        const { LEFT, RIGHT, UP, A, D, W, SPACE } = Phaser.Input.Keyboard.KeyCodes;
        this.leftInput = new MultiKey(scene, [LEFT, A]);
        this.rightInput = new MultiKey(scene, [RIGHT, D]);
        this.jumpInput = new MultiKey(scene, [UP, W, SPACE]);

        // Подключаем update
        this.scene.events.on("update", this.update, this);
    }

    //----------------------------------------- SENSOR COLLIDE ---------------------------------------------------------
    onSensorCollide({ bodyA, bodyB, pair })
    {
        //  Наблюдаем за тем как сенсоры сталкивается со стенами и тп.
        //  Watch for the player colliding with walls/objects on either side and the ground below, so
        //  that we can use that logic inside of update to move the player.
        //  Note: we are using the "pair.separation" here. That number tells us how much bodyA and bodyB
        //  overlap. We want to teleport the sprite away from walls just enough so that the player won't
        //  be able to press up against the wall and use friction to hang in midair. This formula leaves
        //  0.5px of overlap with the sensor so that the sensor will stay colliding on the next tick if
        //  the player doesn't move.

        if ( bodyB.isSensor ) return; // We only care about collisions with physical objects

        if ( bodyA === this.sensors.left )
        {
            this.isTouching.left = true;

            //if (pair.separation > 0.5) this.sprite.x += pair.separation - 0.5;
        }
        else if ( bodyA === this.sensors.right )
        {
            this.isTouching.right = true;
            //if (pair.separation > 0.5) this.sprite.x -= pair.separation - 0.5;
        }
        else if ( bodyA === this.sensors.bottom )
        {
            this.isTouching.ground = true;
        }
        else if ( bodyA === this.sensors.mainBody )
        {
            this.isTouching.mainBody = true;
        }

    }

    //----------------------------------------------- RESET TOUCHING ---------------------------------------------------
    resetTouching()
    {
        this.isTouching.left = false;
        this.isTouching.right = false;
        this.isTouching.ground = false;
        this.isTouching.mainBody = false;
    }

    //--------------------------------------------------- FREEZE -------------------------------------------------------
    freeze() { this.sprite.setStatic(true); }
    freezeOff() { this.sprite.setStatic(false); }

    //--------------------------------------------------- UPDATE -------------------------------------------------------
    update()
    {
        // Если обьект уничтожен ничего оне делаем
        if (this.destroyed) return;

        // Спрайт обьекта
        const sprite = this.sprite;

        // Скорость обьекта
        const velocity = sprite.body.velocity;

        // События клавиатуры
        const isRightKeyDown = this.rightInput.isDown();
        const isLeftKeyDown = this.leftInput.isDown();
        const isJumpKeyDown = this.jumpInput.isDown();

        // Обьект на земле или в воздухе
        const isOnGround = this.isTouching.ground;
        const isInAir = !isOnGround;

        // Adjust the movement so that the player is slower in the air
        const moveForce = isOnGround ? 0.055 : 0.035; //  Скорость X на земле или в воздухе
        const moveForceY = -0.08;

        //----------------------------------------- LISTENING KEYDOWN --------------------------------------------------
        // Если нажата кнопка влево.
        if ( isLeftKeyDown )
        {
            sprite.setFlipX(true);

            // Don't let the player push things left if they in the air
            // Не позволяйте игроку толкать вещи влево, если они в воздухе
            if (!(isInAir && this.isTouching.left)) {
                sprite.applyForce({ x: -moveForce, y: 0 });
            }
        }
        // Или если нажата кнопка вправо.
        else if ( isRightKeyDown )
        {
            sprite.setFlipX(false);

            // Не позволяйте игроку толкать вещи правильно, если они в воздухе
            if (!(isInAir && this.isTouching.right)) {
                sprite.applyForce({ x: moveForce, y: 0 });
            }
        }

        // Limit horizontal speed, without this the player's velocity would just keep increasing to
        // absurd speeds. We don't want to touch the vertical velocity though, so that we don't
        // interfere with gravity.
        if ( velocity.x > 2 ) sprite.setVelocityX(2);
        else if ( velocity.x < -2 ) sprite.setVelocityX(-2);

        // Если нажали прыжок и мы можем прыгать и мы на земле
        if ( isJumpKeyDown && this.bCanJump && isOnGround )
        {
            sprite.anims.stop();
            sprite.anims.play("player-jump-start", true);
            sprite.setVelocityY(-9);

            // С этой хуйне ускорение в прыжке
            //sprite.applyForce({ x: 0, y: moveForceY });

            // Add a slight delay between jumps since the bottom sensor will still collide for a few
            // frames after a jump is initiated
            this.bCanJump = false;
            this.jumpCooldownTimer = this.scene.time.addEvent(
            {
                delay: 250,
                callback: () =>
                {
                    this.bCanJump = true;
                }
            });
        }

        //----------------------------------------- LISTENING SPRITE STATUS --------------------------------------------
        // Если на земле
        if ( isOnGround )
        {
            // Если двигаеться
            if (sprite.body.force.x !== 0)
            {
                sprite.anims.play("player-run", true);
            }
            else
            {
                if ( sprite.anims.isPlaying === false )
                {
                    // Моргает по шансу.
                    let nRand = Phaser.Math.Between(1, 10);

                    if (parseInt(nRand) === 5)
                    {
                        sprite.anims.play("player-idle-blinking", true);
                    }
                    else
                    {
                        sprite.anims.play("player-idle", true);
                    }
                }
                else
                {
                    // Если анимация же проигрываеться не зачем её дублить.
                    // Но может проигрываться другая анимация
                    if ( sprite.anims.currentAnim.key !== "player-jump-start" )
                    {
                        if ( sprite.anims.currentAnim.key !== "player-idle-blinking" &&
                             sprite.anims.currentAnim.key !== "player-idle" )
                        {
                            sprite.anims.stop();
                        }
                    }
                }
            }

            this.sprite.body.friction = this.characteristics.nDefaultFriction;
        }

        // Если в воздухе
        else if ( isInAir )
        {
            if ( sprite.anims.isPlaying === false )
            {
                // Проверяем началась ли анимация (при старте она может быть null исключаем ошибку!)
                if ( sprite.anims.currentAnim !== null )
                {
                    //console.log(sprite.anims.currentAnim.key);
                }
                else
                {
                    sprite.anims.play("player-jump-loop", true);
                }
            }
            else
            {
                if ( sprite.anims.currentAnim.key === "player-run" )
                {
                    //sprite.anims.play("player-jump-loop", true);
                    sprite.anims.play("player-falling-down", true);
                }
            }
        }

        //---------------------------------- LISTENING LEFT & RIGHT SENSORS --------------------------------------------
        // Если касаемся правым или левым сенсором
        if ( this.isTouching.right === true || this.isTouching.left === true )
        {
            if ( !isOnGround ) { this.sprite.body.friction = this.characteristics.nLeftOrRightSensorsFriction; }
        }
    }
}