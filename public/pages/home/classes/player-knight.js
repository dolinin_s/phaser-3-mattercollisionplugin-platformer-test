/** ------------------------------------------------ PLAYER -------------------------------------------------------- **/
class PlayerKnight
{
    constructor(scene, x, y)
    {
        this.scene = scene;
        this.sprite = scene.matter.add.sprite(x, y, 'player_knight', 0).setDepth(25);

        // Animation
        const anims = scene.anims;
        anims.create({
            key: "player-idle",
            frames: anims.generateFrameNames("player_knight", {prefix: 'idle_', start: 0, end: 7}),
            frameRate: 8,
            repeat: -1
        });
        anims.create({
            key: "player-run",
            frames: anims.generateFrameNames("player_knight", {prefix: 'run_', start: 0, end: 7}),
            frameRate: 21,
            repeat: -1
        });
        anims.create({
            key: "player-attack",
            frames: anims.generateFrameNames("player_knight", {prefix: 'attack_', start: 0, end: 7}),
            frameRate: 21,
            repeat: -1
        });
        anims.create({
            key: "player-jump",
            frames: anims.generateFrameNames("player_knight", {prefix: 'jump_', start: 1, end: 1}),
            frameRate: 12,
            repeat: 0
        });

        // Player structure
        const { Body, Bodies } = Phaser.Physics.Matter.Matter; // Native Matter modules
        const { width: w, height: h } = this.sprite;
        const mainBody = Bodies.rectangle(0, 0, 35, 65, { chamfer: { radius: 15 } });
        this.sensors = {
            bottom: Bodies.rectangle(0, h * 0.29, w * 0.25, 5, { isSensor: true }),
            //left: Bodies.rectangle(-w * 0.35, 0, 2, h * 0.5, { isSensor: true }),
            //right: Bodies.rectangle(w * 0.35, 0, 2, h * 0.5, { isSensor: true })
        };
        const compoundBody = Body.create({
            parts: [mainBody, this.sensors.bottom,/*this.sensors.left, this.sensors.right*/],
            frictionStatic: 0,
            frictionAir: 0.001,
            friction: 0.035
        });
        this.sprite
            .setExistingBody(compoundBody)
            .setScale(1)
            .setFixedRotation() // Sets inertia to infinity so the player can't rotate
            .setPosition(x, y);

        // Track which sensors are touching something
        this.isTouching = { ground: false };

        // Jumping is going to have a cooldown
        this.canJump = true;
        this.jumpCooldownTimer = null;
        this.bAnimisFly = false;

        // Before matter's update, reset our record of which surfaces the player is touching.
        scene.matter.world.on("beforeupdate", this.resetTouching, this);

        scene.matterCollision.addOnCollideStart({
            objectA: [this.sensors.bottom],
            callback: this.onSensorCollide,
            context: this
        });
        scene.matterCollision.addOnCollideActive({
            objectA: [this.sensors.bottom],
            callback: this.onSensorCollide,
            context: this
        });

        // Track the keys
        const { LEFT, RIGHT, UP, A, D, W } = Phaser.Input.Keyboard.KeyCodes;
        this.leftInput = new MultiKey(scene, [LEFT, A]);
        this.rightInput = new MultiKey(scene, [RIGHT, D]);
        this.jumpInput = new MultiKey(scene, [UP, W]);

        this.scene.events.on("update", this.update, this);
    }

    //----------------------------------------- SENSOR COLLIDE ---------------------------------------------------------
    onSensorCollide({ bodyA, bodyB, pair })
    {
        // Watch for the player colliding with walls/objects on either side and the ground below, so
        // that we can use that logic inside of update to move the player.
        // Note: we are using the "pair.separation" here. That number tells us how much bodyA and bodyB
        // overlap. We want to teleport the sprite away from walls just enough so that the player won't
        // be able to press up against the wall and use friction to hang in midair. This formula leaves
        // 0.5px of overlap with the sensor so that the sensor will stay colliding on the next tick if
        // the player doesn't move.
        if (bodyB.isSensor) return; // We only care about collisions with physical objects
        if (bodyA === this.sensors.left) {
            this.isTouching.left = true;
            if (pair.separation > 0.5) this.sprite.x += pair.separation - 0.5;
        } else if (bodyA === this.sensors.right) {
            this.isTouching.right = true;
            if (pair.separation > 0.5) this.sprite.x -= pair.separation - 0.5;
        } else if (bodyA === this.sensors.bottom) {
            this.isTouching.ground = true;
        }
    }

    //----------------------------------------------- RESET TOUCHING ---------------------------------------------------
    resetTouching()
    {
        this.isTouching.left = false;
        this.isTouching.right = false;
        this.isTouching.ground = false;
    }

    //--------------------------------------------------- FREEZE -------------------------------------------------------
    freeze() { this.sprite.setStatic(true); }
    freezeOff() { this.sprite.setStatic(false); }
    //--------------------------------------------------- UPDATE -------------------------------------------------------
    update()
    {
        // Если обьект уничтожен ничего оне делаем
        if (this.destroyed) return;

        // Спрайт обьекта
        const sprite = this.sprite;

        // Скорость обьекта
        const velocity = sprite.body.velocity;

        // События клавиатуры
        const isRightKeyDown = this.rightInput.isDown();
        const isLeftKeyDown = this.leftInput.isDown();
        const isJumpKeyDown = this.jumpInput.isDown();

        // Обьект на земле или в воздухе
        const isOnGround = this.isTouching.ground;
        const isInAir = !isOnGround;

        //console.log("isOnGround", isOnGround);

        // Adjust the movement so that the player is slower in the air
        const moveForce = isOnGround ? 0.03 : 0.02;
        const moveForceY = -0.08;

        // Left & Right player move
        if (isLeftKeyDown)
        {
            sprite.setFlipX(false);
            //sprite.play('player-run');

            // Don't let the player push things left if they in the air
           if (!(isInAir && this.isTouching.left)) {
                sprite.applyForce({ x: -moveForce, y: 0 });
            }
        }
        else if (isRightKeyDown)
        {
            sprite.setFlipX(true);
            //sprite.play('player-run');

            // Don't let the player push things right if they in the air
            if (!(isInAir && this.isTouching.right)) {
                sprite.applyForce({ x: moveForce, y: 0 });
            }
        }

        // Limit horizontal speed, without this the player's velocity would just keep increasing to
        // absurd speeds. We don't want to touch the vertical velocity though, so that we don't
        // interfere with gravity.
        if (velocity.x > 2) sprite.setVelocityX(2);
        else if (velocity.x < -2) sprite.setVelocityX(-2);

        // --- Move the player vertically ---
        if ( isJumpKeyDown && this.canJump && isOnGround )
        {
            sprite.anims.stop();
            sprite.setVelocityY(-11);
            // С этой хуйне ускорение в прыжке
            //sprite.applyForce({ x: 0, y: moveForceY });

            // Add a slight delay between jumps since the bottom sensor will still collide for a few
            // frames after a jump is initiated
            this.canJump = false;
            this.jumpCooldownTimer = this.scene.time.addEvent(
                {
                    delay: 250,
                    callback: () => {this.canJump = true; this.bAnimisFly = false}
            });
        }

        // Update the animation/texture based on the state of the player's state
        if (isOnGround)
        {
            if (sprite.body.force.x !== 0)
            {
                sprite.anims.play("player-run", true);
            }
            else
            {
                sprite.anims.play("player-idle", true);
            }
        }
        else if(isInAir)
        {
            if (sprite.anims.currentAnim)
            {
                if (this.bAnimisFly === false)
                {
                    this.bAnimisFly = true;
                    sprite.anims.play("player-jump", true);
                }
            }
        }
        else
        {
            console.log(sprite.body.force.y);
            sprite.anims.stop();
            //sprite.setTexture("player", 10);

        }
    }
}