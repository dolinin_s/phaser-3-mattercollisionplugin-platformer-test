<?php if( !defined('SD') ) exit(0);
/**
 * Created by PhpStorm.
 * User: Sergey Dolinin
 * Date: 31.03.2019
 * Time: 18:21
----------------------------------------------------------- MAIN --------------------------------------------------- **/
function page_Main ()
{
    // Header
    pages_Header
    (
        'Game 1',
        '',
        '',
        ['globals', 'jQuery', 'phaser', 'MatterCollisionPlugin','webfontloader'],
        true
    );

    // Page elements
    page_Content();

    // Footer
    pages_Footer(['globals', 'jQuery', 'phaser', 'MatterCollisionPlugin','webfontloader']);

}

/** ----------------------------------------------------- CONTENT -------------------------------------------------- **/
function page_Content ()
{

}
