<?php
/**
 * Created by PhpStorm.
 * User: Sergey Dolinin
 * Date: 31.03.2019
 * Time: 14:26
 */

//ini_set('error_reporting', E_ALL);
/******************************************************** BASE ********************************************************/
define('SD', true);                                         // Protecting files from direct access
define('BASE_DIR', __DIR__);                                // Base Dir
define('DIR', __DIR__);                                     // Base Dir
define('NR', "\n\r");                                       // NR
define('TAB', "\t");                                        // TAB
date_default_timezone_set('Europe/Moscow');  // Set timezone

/******************************************************* INCLUDE ******************************************************/
include_once 'config.php';
include_once 'core/sd.php';