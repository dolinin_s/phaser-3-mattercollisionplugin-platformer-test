/** -------------------------------------------------- GLOBALS ----------------------------------------------------- **/

let GLOBALS =
{
    CONFIG : null,
    GAME : null,
    SCENE :
    {
        MENU : null,
        LEVEL_TEST : null,
        LEVEL_ONE : null
    },

    /* DIALOG CLASS */
    DIALOG :
    {
        OPENED : null
    },
    PATH :
    {
        SPRITES : "public/pages/home/assets/sprites/",
        MENU : "public/pages/home/assets/menu/",
        BACKGROUNDS : "public/pages/home/assets/backgrounds/",
        TILEMAPS : "public/pages/home/assets/tilemaps/",
        OBJECTS : "public/pages/home/assets/objects/",
        HTML : "public/pages/home/html/"
    },
    PLAYER :
    {
        DEPTH : 250
    }
};