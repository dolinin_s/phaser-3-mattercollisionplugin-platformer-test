<?php if( !defined('SD') ) exit(0);
/**
 * Created by PhpStorm.
 * User: Sergey Dolinin
 * Date: 31.03.2019
 * Time: 17:39
 */

// Global array of errors
global $g_aLastErrors;
$g_aLastErrors = array();

/** ------------------------------------------------ URL SEGMENT -------------------------------------------------------
 * @param $nURLSegment => Segment number
 * @return string
 */
function sd_UrlSegment( $nURLSegment )
{
    $aSegments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
    return isset($aSegments[$nURLSegment]) ? $aSegments[$nURLSegment] : '';
}

/** ----------------------------------------------- LAST URL SEGMENT ---------------------------------------------------
 *  Возвращаем последний сегмент url.
 */
function sd_UrlLastSegment()
{
    $aSegments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
    return end($aSegments);
}

/** ------------------------------------------------- PRINT ARRAY ------------------------------------------------------
 * @param $aArray
 */
function sd_PrintArray( $aArray )
{
    echo '<pre>' . print_r($aArray,true) . '</pre>';
}

/** ------------------------------------------------ INCLUDE PLUGIN ------------------------------------------------ **/
function include_Plugin()
{
    global $SD_GLOBAL;

    // Find all the plugins
    foreach( glob(DIR.'/plugins/*') as $PluginFolder )
    {
        if( is_dir($PluginFolder) )
        {
            // Load index.php plugins
            if (file_exists($PluginFolder . '/index.php'))
            {
                include_once($PluginFolder . '/index.php');
            }

            // Registration plugins
            $cExp = explode('/', $PluginFolder);
            $cExp = end($cExp);
            $aPlugin = array("NAME" => $cExp, "INDEX" => "");
            array_push($SD_GLOBAL['PLUGINS'],$aPlugin);
        }
    }
}

/** -------------------------------------------------- SET LAST ERROR --------------------------------------------------
 * @param $cError
 * @return bool
 */
function sd_SetLastError( $cError )
{
    global $g_aLastErrors, $g_aConfig;
    $g_aLastErrors[] = $cError;

    if ( $g_aConfig['error']['show'] == 1 )
    {
        echo '<div class="global-error">' . $cError . '</div>';
    }

    return false;
}

/**--------------------------------------------- IS TABLE EXISTS -------------------------------------------------------
 * Checks table existence and returns TRUE/FALSE result
 * @param $cTable
 * @return bool
 */
function db_IsTableExists( $cTable )
{
    global $g_aDB;

    // Get all the tables and save them for further use
    if( !isset($g_aDB['tables']) )
    {
        $objResult = db_Query("SHOW TABLES;");
        $g_aDB['tables'] = array();
        if( $objResult!==false )
            foreach( $objResult as $Item )
                foreach( $Item as $SubItem )
                    $g_aDB['tables'][] = $SubItem;
    }

    return array_search($cTable, $g_aDB['tables'])===false ? false : true;
}

/* ############################################ MISC FUNCTION ####################################################### */
/* --------------------------------------------- PRINT ARRAY -------------------------------------------------------- */
function ai_PrintArray( $aArray )
{
    echo '<pre>' . print_r($aArray,true) . '</pre>';
}

//------------------------------------------------- AI INFO ------------------------------------------------------------
function ai_Info( $cInfoToGet )
{
    global $g_aConfig;
    $cProtocol = 'http://';
    if (isset($g_aConfig['protocol'])) $cProtocol = $g_aConfig['protocol'] . '://';

    switch( $cInfoToGet )
    {
        case	'host':			{return $g_aConfig['site_host'];}
        case	'base_dir':		{return BASE_DIR . '/';}
        case	'site_url':		{return $cProtocol . $g_aConfig['site_host'] . '/';}
        case	'view_dir':		{return BASE_DIR . '/view/';}
        case	'view_url':		{return $cProtocol . $g_aConfig['site_host'] . '/view/';}
        case	'plugins_dir':	{return BASE_DIR . '/plugins/';}
        case	'plugins_url':	{return $cProtocol . $g_aConfig['site_host'] . '/plugins/';}
    }

    if (isset($g_aConfig['info'][$cInfoToGet])) return $g_aConfig['info'][$cInfoToGet];
}

//------------------------------------------------------ AI HASH -------------------------------------------------------
function ai_Hash( $cValueToHash )
{
    $cHash = md5( $cValueToHash );

    // Encode
    for( $i=0;$i<31;$i++ )
    {
        $cTmp = sprintf( "%02x", ((hexdec($cHash{$i} . $cHash{$i+1})^156)<<5)&0xff | ((hexdec($cHash{$i} . $cHash{$i+1})^156)>>3) );
        $cHash{$i} = $cTmp{0};
        $cHash{$i+1} = $cTmp{1};
    }

    // Decode DO NOT DELETE!!!
    /* for( $i=30;$i>=0;$i-- )
     {
    	$cTmp = sprintf( "%02x", ((hexdec($cHash{$i} . $cHash{$i+1})>>5)^156)&7 | ((hexdec($cHash{$i} . $cHash{$i+1})<<3)^156)&0xff&(255-7)  );
     	$cHash{$i} = $cTmp{0};
     	$cHash{$i+1} = $cTmp{1};
     }
    */

    return $cHash;
}

//-------------------------------------------------- IS HTTPS ONLY -----------------------------------------------------
function ai_IsHTTPSOnly()
{
    global	$g_aConfig;
    return $g_aConfig['https_only'];
}

//-------------------------------------------------- GENERATE STRING ---------------------------------------------------
/**
 * Функция генратор, возвращает сгенерированную строку нужного размера
 * @param int $length => Длина строки
 * @return string
 */
function ai_GenerateString( $length = 6 )
{
    $randStr = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    $rand = '';
    for( $i = 0; $i < $length; $i++ )
    {
        $key = rand(0, strlen($randStr)-1);
        $rand .= $randStr[$key];
    }
    return $rand;
}
//---------------------------------------------------- GET COOKIE ------------------------------------------------------
function ai_GetInterfaceCookie( $cName, $cGroup='interface' )
{
    if( !isset($_COOKIE[$cGroup]) )return false;
    if( $cName!==false && strpos($_COOKIE[$cGroup], $cName)===false ) return false;
    $aCookie = json_decode($_COOKIE[$cGroup], true);
    if( $cName===false && $aCookie!==NULL ) return $aCookie;
    if( isset( $aCookie[$cName] ) ) return $aCookie[$cName];
    else return false;
}

//---------------------------------------------------- SET COOKIE ------------------------------------------------------
function ai_SetInterfaceCookie( $aData, $cGroup='interface' )
{
    $aCookie = array();
    if( isset($_COOKIE[$cGroup]) )
        $aCookie = json_decode($_COOKIE[$cGroup],true);
    $aCookie['cookie_date'] = date('Y')+1 . date('md');
    foreach( $aData as $key=>$value )
        $aCookie[$key] = $value;
    $cCookie = json_encode($aCookie);
    setcookie ( $cGroup , $cCookie ,time()+ 24*60*60 , '/');
    $_COOKIE[$cGroup] = $cCookie;
}

//------------------------------------------------- UNSET COOKIE -------------------------------------------------------
function ai_UnsetCookie( $cGroup )
{
    setcookie( $cGroup, '', time(), '/' );
}

//---------------------------------------------  DEBUGGING TRACE -------------------------------------------------------
function ai_Trace( $cMessage, $cDirName = false )
{
    if( is_array($cMessage) )ai_Trace(print_r($cMessage,true));	// Convert array to string and trace it

    $hFile = fopen( ( $cDirName===false  ? dirname(__FILE__).'/../log' : $cDirName) . '/log.txt', 'a+' );
    if ($hFile === false) return;
    fwrite( $hFile, "[". date("Y-m-d H:i:s"). "] ". $cMessage . NR );
    fclose( $hFile );
}

//---------------------------------------------- UPDATE CAPTCHA --------------------------------------------------------
function ai_UpdateCaptcha ()
{
    global $g_aConfig;

    for ($i = 0; $i < $g_aConfig['captcha']['chars_count']; $i++)
    {
        $g_aConfig['captcha']['code'] .= substr($g_aConfig['captcha']['allowed_chars'],
            mt_rand(0, strlen($g_aConfig['captcha']['allowed_chars']) - 1), 1);
    }

    setcookie($g_aConfig['captcha']['cookie_title'], ai_Hash($g_aConfig['captcha']['code']));
}