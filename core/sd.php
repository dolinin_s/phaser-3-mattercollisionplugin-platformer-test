<?php if( !defined('SD') ) exit(0);
/**
 * Created by PhpStorm.
 * User: Sergey Dolinin
 * Date: 31.03.2019
 * Time: 14:36
 */
include_once 'function.php';

// Global
$SD_GLOBAL = array("LANG" => "", "PLUGINS" => array());

/** --------------------------------------------------- CORE ------------------------------------------------------- **/
function core_main()
{
    global $SD_GLOBAL;

    // Подключаем плагины
    include_Plugin();

    // Include public base files
    foreach( glob(DIR . '/public/*.php') as $Item ) { include_once($Item); }

    // Include appropriate view file and run page_Main()
    if( sd_UrlSegment(0) == 'plugins' )
    {
        $cViewFile = DIR . $_SERVER['REQUEST_URI'];
    }

    else if ( sd_UrlSegment(0) == 'ru' || sd_UrlSegment(0) == 'en' )
    {
        $SD_GLOBAL['LANG'] = sd_UrlSegment(0);
        if ( sd_UrlSegment(1) == '' )
        {
            $cViewFile = DIR.'/public/pages/home';
        }
        else
        {
            $cViewFile = DIR.'/public/pages/'.sd_UrlSegment(1);
        }
    }

    else if ( sd_UrlSegment(0)!='' )
    {
        $cViewFile = DIR . '/public/pages/' . sd_UrlSegment(0);
    }

    else
    {
        $cViewFile = DIR.'/public/pages/home';
    }

    if ( !file_exists($cViewFile) || !is_dir($cViewFile) )
    {
        $cViewFile.= '.php';
    }
    else
    {
        $cViewFile.= '/index.php';
    }

    if( file_exists($cViewFile) && is_file($cViewFile) )
    {
        include_once($cViewFile);
        if( function_exists('page_Main') ) page_Main();
    }

    else if (file_exists(DIR . '/public/pages/404.php'))
    {
        include_once(DIR . '/public/pages/404.php');
    }
}

/** ------------------------------------------------ CORE START ---------------------------------------------------- **/
core_main();
